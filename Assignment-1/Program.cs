﻿using Assignment_1.Hero;
using Assignment_1.Items;


internal class Program
{
    private static void Main(string[] args)
    {
        Mage mage = new Mage("Randi");
        Ranger ranger = new Ranger("Johanna");
        /*
        Rouge rouge = new Rouge("Karl");

        Weapon wand = new Weapon("Wand", 1, Slot.Weapon, WeaponType.Wand, 2);
        
        Armor cloth = new Armor("Cloth", 1, Slot.Body, ArmorType.Cloth, mage.LevelAttribute);


        Armor leatherLegs = new Armor("Leather", 1, Slot.Legs, ArmorType.Leather, rouge.LevelAttribute);
        Armor leatherBody = new Armor("Leather", 1, Slot.Body, ArmorType.Leather, rouge.LevelAttribute);

        Armor mailLegs = new Armor("Mail", 1, Slot.Legs, ArmorType.Mail, rouge.LevelAttribute);
        Armor mailBody = new Armor("Mail", 1, Slot.Body, ArmorType.Mail, rouge.LevelAttribute);

        // Mage
        Console.WriteLine(mage.Name + " er en Mage med level " + mage.Level + " og dex: " + mage.LevelAttribute.Dexterity + ", int: " + mage.LevelAttribute.Intelligence + ", strength: " + mage.LevelAttribute.Strength + "  med armor: ");
        mage.LevelUp();
        mage.Equip(cloth);
        mage.Equip(wand);
        Console.WriteLine(mage.Name + " er en Mage med level " + mage.Level + " og dex: " + mage.LevelAttribute.Dexterity + ", int: " + mage.LevelAttribute.Intelligence + ", strength: " + mage.LevelAttribute.Strength + "  med armor: " + mage.Equipment[Slot.Body].Name);

        Console.WriteLine("");
        */
        /*
        // Ranger
        Console.WriteLine(ranger.Name + " er en Ranger med level " + ranger.Level + " og dex: " + ranger.LevelAttribute.Dexterity + ", int: " + ranger.LevelAttribute.Intelligence + ", strength: " + ranger.LevelAttribute.Strength);
        ranger.LevelUp();
        Console.WriteLine(ranger.Name + " er en Ranger med level " + ranger.Level + " og dex: " + ranger.LevelAttribute.Dexterity + ", int: " + ranger.LevelAttribute.Intelligence + ", strength: " + ranger.LevelAttribute.Strength);

        Console.WriteLine("");

        // Rouge
        Console.WriteLine(rouge.Name + " er en Rouge med armor: ");
        rouge.LevelUp();
        rouge.Equip(mailBody);
        Console.WriteLine(rouge.Name + " er en Rouge med armor: " + rouge.Equipment[Slot.Body].Name + " på hans "+ rouge.Equipment[Slot.Body].Slot);
        rouge.Equip(leatherLegs);
        Console.WriteLine(rouge.Name + " er en Rouge med armor: " + rouge.Equipment[Slot.Legs].Name + " på hans " + rouge.Equipment[Slot.Legs].Slot);
        rouge.Equip(mailLegs);
        Console.WriteLine(rouge.Name + " er en Rouge med armor: " + rouge.Equipment[Slot.Legs].Name + " på hans " + rouge.Equipment[Slot.Legs].Slot);
        rouge.Equip(leatherBody);
        rouge.Equip(cloth);
        Console.WriteLine(rouge.Name + " er en Rouge med armor: " + rouge.Equipment[Slot.Body].Name + " på hans " + rouge.Equipment[Slot.Body].Slot);

        Console.WriteLine("damage " + mage.getDamage());
        */

        Rouge rouge = new Rouge("Truls");
        int armorAttrbituteStrength = 1;
        int armorAttrbituteDexterity = 1;
        int armorAttrbituteIntelligence = 1;
        HeroAttribute armorAttribute = new(armorAttrbituteStrength, armorAttrbituteDexterity, armorAttrbituteIntelligence);

        Armor mail = new Armor("Mail", 1, Slot.Body, ArmorType.Mail, armorAttribute);
        Weapon sword = new Weapon("Sword", 1, Slot.Weapon, WeaponType.Sword, 3);
        Console.WriteLine(rouge.Name + " er en Rouge med armor: ");
        Console.WriteLine("her");
        rouge.Equip(mail);
        Console.WriteLine(" s: " + rouge.GetTotalAttributes().Strength + " d: " + rouge.GetTotalAttributes().Dexterity + " i: " + rouge.GetTotalAttributes().Intelligence);
        Console.WriteLine(rouge.getDamage() + " " + mail.ArmorAttribute.Strength);
        rouge.Equip(sword);
        Console.WriteLine(" s: " + rouge.GetTotalAttributes().Strength + " d: " + rouge.GetTotalAttributes().Dexterity + " i: " + rouge.GetTotalAttributes().Intelligence);
        Console.WriteLine(rouge.getDamage() + " " + mail.ArmorAttribute.Strength);
        Console.WriteLine("hit");


        Console.WriteLine(rouge.Name + " er en Rouge med armor: " + rouge.Equipment[Slot.Body].Name + " på hans " + rouge.Equipment[Slot.Body].Slot);
        Console.WriteLine(" s: " + rouge.GetTotalAttributes().Strength + " d: " + rouge.GetTotalAttributes().Dexterity + " i: " + rouge.GetTotalAttributes().Intelligence);
        rouge.LevelUp();
        Console.WriteLine(" s: " + rouge.GetTotalAttributes().Strength + " d: " + rouge.GetTotalAttributes().Dexterity + " i: " + rouge.GetTotalAttributes().Intelligence);

        Console.WriteLine(rouge.LevelAttribute.Strength + " " + mail.ArmorAttribute.Strength);
    }
}