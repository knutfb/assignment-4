﻿using System;
namespace Assignment_1.Items
{
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; init; }
        public double WeaponDamage { get; init; } // { set => calcDamage() }

        public Weapon(string name, int requiredLevel, Slot slot, WeaponType weaponType, double weaponDamage) : base(name, requiredLevel, slot)
        {
            WeaponType = weaponType;
            WeaponDamage = weaponDamage;
     
        }

    }
}

