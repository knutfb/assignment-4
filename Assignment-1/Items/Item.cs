﻿using System;
namespace Assignment_1.Items
{
    public abstract class Item
    {
        public string? Name { get; set; }
        public int RequiredLevel { get; set; }
        public Slot Slot;

        
        public Item(string name, int requiredLevel, Slot slot)
        {
            Name = name;
            RequiredLevel = requiredLevel;
            Slot = slot;

        }
    
    }
}

