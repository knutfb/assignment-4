﻿using System;
using Assignment_1.Hero;
namespace Assignment_1.Items
{
    public class Armor : Item
    {
        public ArmorType ArmorType { get; init; }
        public HeroAttribute ArmorAttribute { get; init; }

        public Armor(string name, int requiredLevel, Slot slot, ArmorType armorType, HeroAttribute armorAttribute) : base(name, requiredLevel, slot)
        {
            ArmorType = armorType;
            ArmorAttribute = armorAttribute;
        }

    }
}

