﻿using System;
using Assignment_1.Items;
namespace Assignment_1.Hero
{
    public class Ranger : Hero
    {
        private HeroAttribute RangerAttribute = new HeroAttribute(1, 7, 1);
        private HeroAttribute LevelUpRangerAttribute = new HeroAttribute(1, 5, 1);

        public Ranger(string name) : base(name)
        {
            LevelAttribute ??= RangerAttribute;
            ValidWeaponTypes = new List<WeaponType>() { WeaponType.Bow };
            ValidArmorTypes = new List<ArmorType>() {ArmorType.Leather, ArmorType.Mail };

        }

        public override void LevelUp()
        {
            RangerAttribute.LevelUpHeroAttribute(LevelUpRangerAttribute);
            base.LevelUp();
        }

        public override double getDamage()
        {
            return base.getDamage() * (1 + (double)LevelAttribute.Dexterity / 100);
        }
    }
}

