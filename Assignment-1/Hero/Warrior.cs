﻿using System;
using Assignment_1.Items;
namespace Assignment_1.Hero
{
    public class Warrior : Hero
    {
        HeroAttribute WarriorAttribute = new HeroAttribute(5, 2, 1);
        HeroAttribute LevelUpWarriorAttribute = new HeroAttribute(3, 2, 1);

        public Warrior(string name) : base(name)
        {
            LevelAttribute ??= WarriorAttribute;
            ValidArmorTypes = new List<ArmorType>() { ArmorType.Mail, ArmorType.Plate };
            ValidWeaponTypes = new List<WeaponType>() { WeaponType.Axe, WeaponType.Hammer, WeaponType.Sword };
        }

        public override void LevelUp()
        {
            WarriorAttribute.LevelUpHeroAttribute(LevelUpWarriorAttribute);
            base.LevelUp();
        }
        public override double getDamage()
        {
            if (LevelAttribute == null)
            {
                throw new Exception("Null");
            }
            return base.getDamage() * (1 + (double)LevelAttribute.Strength / 100);
        }
    }
    
}

