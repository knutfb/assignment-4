﻿using System;
using Assignment_1.Items;
namespace Assignment_1.Hero
{
    public class Rouge : Hero
    {
        private HeroAttribute RougeAttribute = new HeroAttribute(2, 6, 1);
        private HeroAttribute LevelUpRougeAttribute = new HeroAttribute(1, 4, 1);

        public Rouge(string name) : base(name)
        {
            LevelAttribute ??= RougeAttribute;
            ValidArmorTypes = new List<ArmorType>() { ArmorType.Leather, ArmorType.Mail };
            ValidWeaponTypes = new List<WeaponType>() { WeaponType.Dagger, WeaponType.Sword };
        }

        public override void LevelUp()
        {
            RougeAttribute.LevelUpHeroAttribute(LevelUpRougeAttribute);
            base.LevelUp();
        }
        public override double getDamage()
        {
            return base.getDamage() * (1 + (double)LevelAttribute.Dexterity / 100);
        }
    }
}

