﻿using System;
using Assignment_1.Items;

namespace Assignment_1.Hero
{
    public class Mage : Hero
    {
        private HeroAttribute MageAttribute = new (1, 1, 8);
        private HeroAttribute LevelUpMageAttribute = new(1, 1, 5);

        
        public Mage(string name) : base(name)
        {
            LevelAttribute ??= MageAttribute;
            ValidWeaponTypes = new List<WeaponType>() { WeaponType.Staff, WeaponType.Wand };
            ValidArmorTypes = new List<ArmorType>() { ArmorType.Cloth };
        }
        public override void LevelUp()
        {
            MageAttribute.LevelUpHeroAttribute(LevelUpMageAttribute);
            base.LevelUp();

        }

        public override double getDamage()
        {
            return base.getDamage() * (1 + (double)LevelAttribute.Intelligence / 100);
        }

    }
}

