﻿using System;
using System.Text;
using Assignment_1.Items;
using Assignment_1.Exceptions;
namespace Assignment_1.Hero

{
    public abstract class Hero
    {
        public string? Name { get; init; }
        public int Level { get; set; } = 1;
        public HeroAttribute? TotalAttributes { get; set; }
        public HeroAttribute LevelAttribute{ get; set; }
        public Dictionary<Slot, Item?> Equipment { get; set; }
        public List<WeaponType>? ValidWeaponTypes { get; set; }
        public List<ArmorType>? ValidArmorTypes { get; set; }



        public Hero(string name)
        {
            Equipment = new Dictionary<Slot, Item?>() { { Slot.Body, null }, { Slot.Head, null }, { Slot.Legs, null }, { Slot.Weapon, null } };
            Name = name;
        }

        public virtual void LevelUp()
        {
            Level += 1;
            Console.WriteLine("Leveled up " +  this.Name);
        }

        public virtual void Equip(Armor armor) 
        {
            
            if (ValidArmorTypes.Contains(armor.ArmorType) && (Level >= armor.RequiredLevel))
            {
                Equipment[armor.Slot] = armor;
            } else
            {
                throw new InvalidArmorException("Clould not equip the selected armor");
            }
        }

        public virtual void Equip(Weapon weapon)
        {
            if (ValidWeaponTypes.Contains(weapon.WeaponType) && (Level >= weapon.RequiredLevel))
            {
                Equipment[Slot.Weapon] = weapon;
            }
            else
            {
                throw new InvalidWeaponException("The weapon can not be equipped for such a role or level");
            }
        }

        public virtual double getDamage()
        {
            
            if (Equipment == null || Equipment[Slot.Weapon] == null)
            {
                return 1;
            }
            else
            {
                return ((Weapon)Equipment[Slot.Weapon]).WeaponDamage;
            }
        }

        public virtual HeroAttribute GetTotalAttributes()
        {
            //if (Equipment == null || LevelAttribute == null) { return new HeroAttribute(0, 0, 0); }

            HeroAttribute TotalAttribute = LevelAttribute;
            foreach (var armor in Equipment)
            {
                if (armor.Key is not Slot.Weapon && armor.Value != null)
                {
                    TotalAttribute += ((Armor)armor.Value).ArmorAttribute;
                }
            }
 
            return TotalAttribute;

        }
        
      
         



        public void Display()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Name: " + Name);
            sb.AppendLine("Class: not implemented");
            sb.AppendLine("Level: " + Level);
            sb.AppendLine("Total strength: " + TotalAttributes.Strength);
            sb.AppendLine("Total dexterity: " + TotalAttributes.Dexterity);
            sb.AppendLine("Total intelligence: " + TotalAttributes.Intelligence);
            sb.AppendLine("Damage: " + getDamage());

        }
    }
}

