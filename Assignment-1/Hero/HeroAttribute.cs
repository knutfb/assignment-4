﻿using System;
namespace Assignment_1.Hero
{
    public class HeroAttribute
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        public HeroAttribute()
        {
            Strength = 0;
            Dexterity = 0;
            Intelligence = 0;
        }

        public HeroAttribute(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }


        //Implement adding two instances and return sum (or +1)
        public void LevelUpHeroAttribute(HeroAttribute levelUpAttribute)
        {
                Strength += levelUpAttribute.Strength;
                Dexterity += levelUpAttribute.Dexterity;
                Intelligence += levelUpAttribute.Intelligence;
        }

        public override bool Equals(object? obj)
        {
            return obj is HeroAttribute attribute &&
                   Strength == attribute.Strength &&
                   Dexterity == attribute.Dexterity &&
                   Intelligence == attribute.Intelligence;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Strength, Dexterity, Intelligence);
        }

        public static HeroAttribute operator +(HeroAttribute lhs, HeroAttribute rhs)
        {
            return new HeroAttribute(
                lhs.Strength + rhs.Strength,
                lhs.Dexterity + rhs.Dexterity,
                lhs.Intelligence + rhs.Intelligence
                );
            
        }

    }
}

