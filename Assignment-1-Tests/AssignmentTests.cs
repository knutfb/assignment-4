﻿using Assignment_1.Hero;
using Assignment_1.Items;
using Assignment_1.Exceptions;
namespace Assignment_1_Tests;

public class AssignmentTests
{
    [Fact]
    public void CreateHeroWithCorrectName()
    {
        // Arrange
        Mage mage = new Mage("Randi");
        string expectedName = "Randi";
        // Act
        string actualName = mage.Name;
        // Assert 
        Assert.Equal(expectedName, actualName);
    }
    [Fact]
    public void CreateHeroWithCorrectLevel()
    {
        // Arrange
        Mage mage = new Mage("Randi");
        int expectedLevel = 1;
        // Act
        int actualLevel = mage.Level;
        // Assert 
        Assert.Equal(expectedLevel, actualLevel);
    }
    [Fact]
    public void CreateHeroWithCorrectStrength()
    {
        // Arrange
        Mage mage = new Mage("Randi");
        int expectedStrength = 1;
        // Act
        int actualStrength = mage.LevelAttribute.Strength;
        // Assert 
        Assert.Equal(expectedStrength, actualStrength);
    }
    [Fact]
    public void CreateHeroWithCorrectDexterity()
    {
        // Arrange
        Mage mage = new Mage("Randi");
        int expectedDexterity = 1;
        // Act
        int actualDexterity = mage.LevelAttribute.Dexterity;
        // Assert 
        Assert.Equal(expectedDexterity, actualDexterity);
    }
    [Fact]
    public void CreateHeroWithCorrectIntelligence()
    {
        // Arrange
        Mage mage = new Mage("Randi");
        int expectedIntelligence = 8;
        // Act
        int actualIntelligence = mage.LevelAttribute.Intelligence;
        // Assert 
        Assert.Equal(expectedIntelligence, actualIntelligence);
    }


// MAGE 
    [Fact]
    public void LevelUpMageToCorrectLevel()
    {
        // Arrange
        Mage mage = new Mage("Randi");
        mage.LevelUp();
        int expectedLevel = 2;
        // Act
        int actualLevel = mage.Level;
        // Assert 
        Assert.Equal(expectedLevel, actualLevel);
    }
    [Fact]
    public void LevelUpMageToCorrectStrength()
    {
        // Arrange
        Mage mage = new Mage("Randi");
        mage.LevelUp();
        int expectedStrength = 2;
        // Act
        int actualStrength = mage.LevelAttribute.Strength;
        // Assert 
        Assert.Equal(expectedStrength, actualStrength);
    }
    [Fact]
    public void LevelUpMageToCorrectDexterity()
    {
        // Arrange
        Mage mage = new Mage("Randi");
        mage.LevelUp();
        int expectedDexterity = 2;
        // Act
        int actualDexterity = mage.LevelAttribute.Dexterity;
        // Assert 
        Assert.Equal(expectedDexterity, actualDexterity);
    }
    [Fact]
    public void LevelUpMageToCorrectIntelligence()
    {
        // Arrange
        Mage mage = new Mage("Randi");
        mage.LevelUp();
        int expectedIntelligence = 13;
        // Act
        int actualIntelligence = mage.LevelAttribute.Intelligence;
        // Assert 
        Assert.Equal(expectedIntelligence, actualIntelligence);
    }


// RANGER
    [Fact]
    public void LevelUpRangerToCorrectLevel()
    {
        // Arrange
        Ranger ranger = new Ranger("Roger");
        ranger.LevelUp();
        int expectedLevel = 2;
        // Act
        int actualLevel = ranger.Level;
        // Assert 
        Assert.Equal(expectedLevel, actualLevel);
    }
    [Fact]
    public void LevelUpRangerToCorrectStrength()
    {
        // Arrange
        Ranger ranger = new Ranger("Roger");
        ranger.LevelUp();
        int expectedStrength = 2;
        // Act
        int actualStrength = ranger.LevelAttribute.Strength;
        // Assert 
        Assert.Equal(expectedStrength, actualStrength);
    }
    [Fact]
    public void LevelUpRangerToCorrectDexterity()
    {
        // Arrange
        Ranger ranger = new Ranger("Roger");
        ranger.LevelUp();
        int expectedDexterity = 12;
        // Act
        int actualDexterity = ranger.LevelAttribute.Dexterity;
        // Assert 
        Assert.Equal(expectedDexterity, actualDexterity);
    }
    [Fact]
    public void LevelUpRangerToCorrectIntelligence()
    {
        // Arrange
        Ranger ranger = new Ranger("Roger");
        ranger.LevelUp();
        int expectedIntelligence = 2;
        // Act
        int actualIntelligence = ranger.LevelAttribute.Intelligence;
        // Assert 
        Assert.Equal(expectedIntelligence, actualIntelligence);
    }


// ROUGE
    [Fact]
    public void LevelUpRougeToCorrectLevel()
    {
        // Arrange
        Rouge rouge = new Rouge("RudolfBlodstrupmoen");
        rouge.LevelUp();
        int expectedLevel = 2;
        // Act
        int actualLevel = rouge.Level;
        // Assert 
        Assert.Equal(expectedLevel, actualLevel);
    }
    [Fact]
    public void LevelUpRougeToCorrectStrength()
    {
        // Arrange
        Rouge rouge = new Rouge("RudolfBlodstrupmoen");
        rouge.LevelUp();
        int expectedStrength = 3;
        // Act
        int actualStrength = rouge.LevelAttribute.Strength;
        // Assert 
        Assert.Equal(expectedStrength, actualStrength);
    }
    [Fact]
    public void LevelUpRougeToCorrectDexterity()
    {
        // Arrange
        Rouge rouge = new Rouge("RudolfBlodstrupmoen");
            rouge.LevelUp();
        int expectedDexterity = 10;
        // Act
        int actualDexterity = rouge.LevelAttribute.Dexterity;
        // Assert 
        Assert.Equal(expectedDexterity, actualDexterity);
    }
    [Fact]
    public void LevelUpRougeToCorrectIntelligence()
    {
        // Arrange
        Rouge rouge = new Rouge("RudolfBlodstrupmoen");
        rouge.LevelUp();
        int expectedIntelligence = 2;
        // Act
        int actualIntelligence = rouge.LevelAttribute.Intelligence;
        // Assert 
        Assert.Equal(expectedIntelligence, actualIntelligence);
    }


    // WARRIOR
    [Fact]
    public void LevelUpWarriorToCorrectLevel()
    {
        // Arrange
        Warrior warrior = new Warrior("Geir");
        warrior.LevelUp();
        int expectedLevel = 2;
        // Act
        int actualLevel = warrior.Level;
        // Assert 
        Assert.Equal(expectedLevel, actualLevel);
    }
    [Fact]
    public void LevelUpWarriorToCorrectStrength()
    {
        // Arrange
        Warrior warrior = new Warrior("Geir");
        warrior.LevelUp();
        int expectedStrength = 8;
        // Act
        int actualStrength = warrior.LevelAttribute.Strength;
        // Assert 
        Assert.Equal(expectedStrength, actualStrength);
    }
    [Fact]
    public void LevelUpWarriorToCorrectDexterity()
    {
        // Arrange
        Warrior warrior = new Warrior("Geir");
        warrior.LevelUp();
        int expectedDexterity = 4;
        // Act
        int actualDexterity = warrior.LevelAttribute.Dexterity;
        // Assert 
        Assert.Equal(expectedDexterity, actualDexterity);
    }
    [Fact]
    public void LevelUpWarriorToCorrectIntelligence()
    {
        // Arrange
        Warrior warrior = new Warrior("Geir");
        warrior.LevelUp();
        int expectedIntelligence = 2;
        // Act
        int actualIntelligence = warrior.LevelAttribute.Intelligence;
        // Assert 
        Assert.Equal(expectedIntelligence, actualIntelligence);
    }

// WEAPON
    [Fact]
    public void CreateWeaponWithCorrectName()
    {
        // Arrange
        Weapon weapon = new Weapon("Axe", 1, Slot.Weapon, WeaponType.Axe, 2);
        string expectedWeaponName = "Axe";
        // Act
        string actualWeaponName = weapon.Name;
        // Assert 
        Assert.Equal(expectedWeaponName, actualWeaponName);
    }
    [Fact]
    public void CreateWeaponWithCorrectLevel()
    {
        // Arrange
        Weapon weapon = new Weapon("Axe", 1, Slot.Weapon, WeaponType.Axe, 2);
        int expectedWeaponLevel = 1;
        // Act
        int actualWeaponLevel = weapon.RequiredLevel;
        // Assert 
        Assert.Equal(expectedWeaponLevel, actualWeaponLevel);
    }
    [Fact]
    public void CreateWeaponWithCorrectWeaponSlot()
    {
        // Arrange
        Weapon weapon = new Weapon("Axe", 1, Slot.Weapon, WeaponType.Axe, 2);
        Slot expectedWeaponSlot = Slot.Weapon;
        // Act
        Slot actualWeaponSlot = weapon.Slot;
        // Assert 
        Assert.Equal(expectedWeaponSlot, actualWeaponSlot);
    }
    [Fact]
    public void CreateWeaponWithCorrectWeaponType()
    {
        // Arrange
        Weapon weapon = new Weapon("Axe", 1, Slot.Weapon, WeaponType.Axe, 2);
        WeaponType expectedWeaponType = WeaponType.Axe;
        // Act
        WeaponType actualWeaponType = weapon.WeaponType;
        // Assert 
        Assert.Equal(expectedWeaponType, actualWeaponType);
    }
    [Fact]
    public void CreateWeaponWithCorrectDamage()
    {
        // Arrange
        Weapon weapon = new Weapon("Axe", 1, Slot.Weapon, WeaponType.Axe, 2);
        int expectedWeaponDamage = 2;
        // Act
        double actualWeaponDamage = weapon.WeaponDamage;
        // Assert 
        Assert.Equal(expectedWeaponDamage, actualWeaponDamage);
    }


// ARMOR
    [Fact]
    public void CreateArmorWithCorrectName()
    {
        // Arrange
        int armorAttrbituteStrength = 0;
        int armorAttrbituteDexterity = 0;
        int armorAttrbituteIntelligence = 0;
        HeroAttribute armorAttribute = new(armorAttrbituteStrength, armorAttrbituteDexterity, armorAttrbituteIntelligence);
        Armor armor = new Armor("Mail", 2, Slot.Body, ArmorType.Mail,armorAttribute);
        string expectedArmorName = "Mail";

        // Act
        string actualArmorName = armor.Name;

        // Assert 
        Assert.Equal(expectedArmorName, actualArmorName);
    }
    [Fact]
    public void CreateArmorWithCorrectLevel()
    {
        // Arrange
        int armorAttrbituteStrength = 0;
        int armorAttrbituteDexterity = 0;
        int armorAttrbituteIntelligence = 0;
        HeroAttribute armorAttribute = new(armorAttrbituteStrength, armorAttrbituteDexterity, armorAttrbituteIntelligence);
        Armor armor = new Armor("Mail", 2, Slot.Body, ArmorType.Mail, armorAttribute);
        int expectedArmorLevel = 2;

        // Act
        int actualArmorLevel = armor.RequiredLevel;

        // Assert 
        Assert.Equal(expectedArmorLevel, actualArmorLevel);
    }
    [Fact]
    public void CreateArmorWithCorrectSlot()
    {
        // Arrange
        int armorAttrbituteStrength = 0;
        int armorAttrbituteDexterity = 0;
        int armorAttrbituteIntelligence = 0;
        HeroAttribute armorAttribute = new(armorAttrbituteStrength, armorAttrbituteDexterity, armorAttrbituteIntelligence);
        Armor armor = new Armor("Mail", 2, Slot.Body, ArmorType.Mail, armorAttribute);
        Slot expectedArmorSlot = Slot.Body;

        // Act
        Slot actualArmorSlot = armor.Slot;

        // Assert 
        Assert.Equal(expectedArmorSlot, actualArmorSlot);
    }
    [Fact]
    public void CreateArmorWithCorrectArmorType()
    {
        // Arrange
        int armorAttrbituteStrength = 0;
        int armorAttrbituteDexterity = 0;
        int armorAttrbituteIntelligence = 0;
        HeroAttribute armorAttribute = new(armorAttrbituteStrength, armorAttrbituteDexterity, armorAttrbituteIntelligence);
        Armor armor = new Armor("Mail", 2, Slot.Body, ArmorType.Mail, armorAttribute);
        ArmorType expectedArmorType = ArmorType.Mail;

        // Act
        ArmorType actualArmorType = armor.ArmorType;

        // Assert 
        Assert.Equal(expectedArmorType, actualArmorType);
    }
    [Fact]
    public void CreateArmorWithCorrectAttributes()
    {
        // Arrange
        int armorAttrbituteStrength = 0;
        int armorAttrbituteDexterity = 0;
        int armorAttrbituteIntelligence = 0;
        HeroAttribute armorAttribute = new(armorAttrbituteStrength, armorAttrbituteDexterity, armorAttrbituteIntelligence);
        Armor armor = new Armor("Mail", 2, Slot.Body, ArmorType.Mail, armorAttribute);

        // Act
        HeroAttribute expectedArmorAttribute = armorAttribute;
        HeroAttribute actualArmorAttribute = armor.ArmorAttribute;

        // Assert 
        Assert.Equal(expectedArmorAttribute, actualArmorAttribute);
    }


// EQUIP WEAPON
    [Fact]
    public void EquipInvalidWeapon()
    {
        // Arrange
        Mage mage = new Mage("Randi");
        Weapon weapon = new Weapon("Axe", 1, Slot.Weapon, WeaponType.Axe, 2);

        // Act & Assert 
        Assert.Throws<InvalidWeaponException>(() => mage.Equip(weapon));
    }
    [Fact]
    public void EquipWeaponTooHighLevel()
    {
        // Arrange
        Warrior warrior = new Warrior("KnutMedTrud");
        Weapon weapon = new Weapon("Axe", 9, Slot.Weapon, WeaponType.Axe, 2);

        // Act & Assert 
        Assert.Throws<InvalidWeaponException>(() => warrior.Equip(weapon));
    }


    // EQUIP ARMOR
    [Fact]
    public void EquipInvalidArmor()
    {
        // Arrange
        Mage mage = new Mage("Randi");

        int armorAttrbituteStrength = 0;
        int armorAttrbituteDexterity = 0;
        int armorAttrbituteIntelligence = 0;
        HeroAttribute armorAttribute = new(armorAttrbituteStrength, armorAttrbituteDexterity, armorAttrbituteIntelligence);

        Armor armor = new Armor("Mail", 3, Slot.Body, ArmorType.Mail, armorAttribute);

        // Act & Assert 
        Assert.Throws<InvalidArmorException>(() => mage.Equip(armor));
    }
    
    [Fact]
    public void EquipArmorTooHighLevel()
    {
        // Arrange
        Warrior warrior = new Warrior("KnutMedTrud");

        int armorAttrbituteStrength = 0;
        int armorAttrbituteDexterity = 0;
        int armorAttrbituteIntelligence = 0;
        HeroAttribute armorAttribute = new(armorAttrbituteStrength, armorAttrbituteDexterity, armorAttrbituteIntelligence);

        Armor armor = new Armor("Mail", 3, Slot.Body, ArmorType.Mail, armorAttribute);

        // Act & Assert 
        Assert.Throws<InvalidArmorException>(() => warrior.Equip(armor));
    }


    // Total attributes
    [Fact]
    public void CorrectTotalAttributesNoEquipment()
    {
        // Arrange
        Rouge rouge = new Rouge("Truls");
     
        // Act
        HeroAttribute expectedTotalAttribute = rouge.LevelAttribute;
        HeroAttribute actualTotalAttributes = rouge.GetTotalAttributes();
        Console.WriteLine(expectedTotalAttribute + " og "+ actualTotalAttributes);
        
        // Assert 
        Assert.Equal(expectedTotalAttribute, actualTotalAttributes);
    }
    
    [Fact]
    public void CorrectTotalAttributesOneArmorPiece()
    {
        // Arrange
        Rouge rouge = new Rouge("Truls");

        int armorAttrbituteStrength = 1;
        int armorAttrbituteDexterity = 1;
        int armorAttrbituteIntelligence = 1;
        HeroAttribute armorAttribute = new(armorAttrbituteStrength, armorAttrbituteDexterity, armorAttrbituteIntelligence);
        Armor mail = new Armor("Mail", 1, Slot.Body, ArmorType.Mail, armorAttribute);

        // Act
        HeroAttribute expectedTotalAttribute = rouge.LevelAttribute + mail.ArmorAttribute;
        rouge.Equip(mail);
        HeroAttribute actualTotalAttributes = rouge.GetTotalAttributes();

        // Assert 
        Assert.Equal(expectedTotalAttribute, actualTotalAttributes);
    }

    [Fact]
    public void CorrectTotalAttributesTwoArmorPieces()
    {
        // Arrange
        Rouge rouge = new Rouge("Truls");

        int armor1AttrbituteStrength = 1;
        int armor1AttrbituteDexterity = 1;
        int armor1AttrbituteIntelligence = 1;
        HeroAttribute armor1Attribute = new(armor1AttrbituteStrength, armor1AttrbituteDexterity, armor1AttrbituteIntelligence);
        Armor mail = new Armor("Mail", 1, Slot.Body, ArmorType.Mail, armor1Attribute);

        int armor2AttrbituteStrength = 2;
        int armor2AttrbituteDexterity = 2;
        int armor2AttrbituteIntelligence = 2;
        HeroAttribute armor2Attribute = new(armor2AttrbituteStrength, armor2AttrbituteDexterity, armor2AttrbituteIntelligence);
        Armor leather = new Armor("Leather", 1, Slot.Legs, ArmorType.Leather, armor2Attribute);

        // Act
        HeroAttribute expectedTotalAttribute = rouge.LevelAttribute + mail.ArmorAttribute + leather.ArmorAttribute;
        rouge.Equip(mail);
        rouge.Equip(leather);
        HeroAttribute actualTotalAttributes = rouge.GetTotalAttributes();

        // Assert 
        Assert.Equal(expectedTotalAttribute, actualTotalAttributes);
    }

    [Fact]
    public void CorrectTotalAttributesReplacedArmor()
    {
        // Arrange
        Rouge rouge = new Rouge("Truls");

        int armor1AttrbituteStrength = 1;
        int armor1AttrbituteDexterity = 1;
        int armor1AttrbituteIntelligence = 1;
        HeroAttribute armor1Attribute = new(armor1AttrbituteStrength, armor1AttrbituteDexterity, armor1AttrbituteIntelligence);
        Armor mail = new Armor("Mail", 1, Slot.Body, ArmorType.Mail, armor1Attribute);

        int armor2AttrbituteStrength = 2;
        int armor2AttrbituteDexterity = 2;
        int armor2AttrbituteIntelligence = 2;
        HeroAttribute armor2Attribute = new(armor2AttrbituteStrength, armor2AttrbituteDexterity, armor2AttrbituteIntelligence);
        Armor leather = new Armor("Leather", 1, Slot.Body, ArmorType.Leather, armor2Attribute);

        // Act
        HeroAttribute expectedTotalAttribute = rouge.LevelAttribute + mail.ArmorAttribute;
        rouge.Equip(mail);
        HeroAttribute actualTotalAttributes = rouge.GetTotalAttributes();

        // Assert 
        Assert.Equal(expectedTotalAttribute, actualTotalAttributes);
    }


    // Calculate damage
    [Fact]
    public void CorrectDamageNoWeaponEqquiped()
    {
        // Arrange
        Warrior warrior = new Warrior("Joachim");

        // Act
        double expectedDamage = (1 + 0.05);
        double actualDamage = warrior.getDamage();

        // Assert 
        Assert.Equal(expectedDamage, actualDamage);
    }

    [Fact]
    public void CorrectDamageOneWeaponEquiped()
    {
        // Arrange
        Warrior warrior = new Warrior("Joachim");
        Weapon sword = new Weapon("Sword", 1, Slot.Weapon, WeaponType.Sword, 2);
        double expectedDamage = 2 * (1 + 0.05);

        // Act
        warrior.Equip(sword);
        double actualDamage = warrior.getDamage();

        // Assert 
        Assert.Equal(expectedDamage, actualDamage);
    }

    [Fact]
    public void CorrectDamageReplacedWeaponEquiped()
    {
        // Arrange
        Warrior warrior = new Warrior("Joachim");
        Weapon sword = new Weapon("Sword", 1, Slot.Weapon, WeaponType.Sword, 1);
        Weapon axe = new Weapon("Axe", 1, Slot.Weapon, WeaponType.Axe, 2);
        double expectedDamageAxe = 2 * (1 + 0.05);


        // Act
        warrior.Equip(sword);
        warrior.Equip(axe);
        double actualDamage = warrior.getDamage();

        // Assert 
        Assert.Equal(expectedDamageAxe, actualDamage);
    }

    [Fact]
    public void CorrectDamageWithWeaponAndArmorEquiped()
    {
        // Arrange
        Warrior warrior = new Warrior("Joachim");

        Weapon sword = new Weapon("Sword", 1, Slot.Weapon, WeaponType.Sword, 2);

        int armor1AttrbituteStrength = 1;
        int armor1AttrbituteDexterity = 0;
        int armor1AttrbituteIntelligence = 0;
        HeroAttribute armor1Attribute = new(armor1AttrbituteStrength, armor1AttrbituteDexterity, armor1AttrbituteIntelligence);
        Armor mail = new Armor("Mail", 1, Slot.Body, ArmorType.Mail, armor1Attribute);

        double expectedDamage = 2 * (1 + 0.05);

        // Act
        warrior.Equip(sword);
        warrior.Equip(mail);
        double actualDamage = warrior.getDamage();

        // Assert 
        Assert.Equal(expectedDamage, actualDamage);
    }


}
